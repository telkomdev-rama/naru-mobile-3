import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { Container, Content, Picker, Button, Text } from "native-base";
import Expo from "expo";
import Intro from "./src/screen/index.js";

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("native-base/Fonts/Ionicons.ttf"),
      Ubuntu_Regular: require("./img/fonts/Ubuntu-Regular.ttf"),
      Ubuntu_Light: require("./img/fonts/Ubuntu-Light.ttf"),
      Gotham_Medium: require("./img/fonts/Gotham-Medium.ttf"),
      Gotham_Light: require("./img/fonts/Gotham-Light.otf"),
      Gotham_Bold: require("./img/fonts/GothamRounded-Bold.otf")
    });
    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (

      <Intro />


    );

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
