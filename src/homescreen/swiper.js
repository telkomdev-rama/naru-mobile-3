import React, { Component } from 'react';
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel';

const datacarousel = [
  {
      "id": 339964,
      "imagePath": "https://temanberbagi.s3.amazonaws.com/promo/a2bdb8de-ea82-4f8e-bc5d-cffc69bf776f.png",
  },
//   {
//       "id": 315635,
//       "imagePath": "https://image.tmdb.org/t/p/w780/fn4n6uOYcB6Uh89nbNPoU2w80RV.jpg",
//   },
//   {
//       "id": 339403,
//       "title": "Baby Driver",
//       "subtitle": "More than just a trend",
//       "imagePath": "https://image.tmdb.org/t/p/w780/xWPXlLKSLGUNYzPqxDyhfij7bBi.jpg",
//   },
];

export default class Swiper extends Component {
    render() {
        return(
            <SwipeableParallaxCarousel
                data={datacarousel}
                navigation={true}
                navigationType={'dots'}
                height={150}
            />
        );
    }
}