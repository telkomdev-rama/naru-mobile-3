import React from "react";
import { Image, AsyncStorage, TouchableOpacity } from "react-native";
import Expo from "expo";
import {
  Container,
  Header,
  Content,
  Left,
  Right,
  Body,
  Title,
  Button,
  Icon,
  Item,
  Input,
  Card,
  CardItem,
  Footer,
  FooterTab,
  Text
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index"

export default class Promo extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      promo: [],
      isReady: false,
    }
  }
  componentDidMount() {
    AsyncStorage.getItem("bumnId", (error, result) => {
      fetch("http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/listpromos/", {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
        .then(response => response.json())
        .then((data) => {
          if (data.length > 0) {
            this.setState({ promo: data });
          }
          this.setState({ isReady: true });
        })
    })
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <Container>
        <AppHeader title="Promo" />
        <Header searchBar rounded style={{ backgroundColor: 'white' }}>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Cari" />
          </Item>
        </Header>
        <Content>
          {this.state.promo.map((item, idx) => {
            return (
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita : item._id })} >
              <Card key={idx} >
                <CardItem cardBody>
                  <Image source={{uri: item.listNewsPromoImage[0]}} style={{height: 180, width: null, flex: 1, borderRadius: 10}} />
                </CardItem>
              </Card>
              </TouchableOpacity>
            )
          })}
        </Content>
        <AppFooter navigation={this.props.navigation} />
      </Container>
    );
  }

};